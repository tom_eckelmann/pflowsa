"""
Traceback, and filter reachable points
"""

import numpy as np
from operator import itemgetter


def trace_opacities(dbundles):
    """
    Computes tracebackpoints, opacities.

    dbundles -- contribution matrix

    Returns advanced contribution and reachable points traceback matrix.
    """
    # m, n is actually m + 1, n + 1, because of shape counting zero
    m, n = dbundles.shape
    # hold all used dways
    tracedbundles = np.empty_like(dbundles, dtype=np.ndarray)
    # point values
    pval = np.zeros_like(dbundles, dtype=float)
    # bottom right corner start opacity 1.0
    pval[m - 1, n - 1] = 1.0
    # begin at bottom row
    for i in range(m - 1, -1, -1):
        for j in range(n - 1, -1, -1):
            if pval[i, j] == 0.0:
                continue
            else:
                threshold = 0.0
                bundles = []
                # sort by maximum dbvalue
                dbmaxsorted = sorted(dbundles[i, j],
                                     key=itemgetter(1), reverse=True)
            # add dways until a threshold of 0.95 reached
            for direction, dbvalue in dbmaxsorted:
                if threshold <= 0.95:
                    if direction == -2:
                        pass
                    elif direction > -1:
                        inext = i - 1
                        jnext = j - direction
                    else:
                        assert False, "wrong direction while tracing?!"
                    opacity = dbvalue * pval[i, j]
                    pval[inext, jnext] += opacity
                    bundles.append((direction, dbvalue, opacity))
                    threshold += dbvalue
            tracedbundles[i, j] = bundles
        # normalize
        nextrowsum = np.sum(pval[i - 1, :])
        pval[i - 1, :] = pval[i - 1, :] / nextrowsum

        # filter active points by point with max value
        nextrowmax = np.amax(pval[i - 1, :])
        pval[i - 1, :] = pval[i - 1, :] / nextrowmax
        # points with less than 1% contribution are filtered out
        pval[i - 1, :][pval[i - 1, :] < 0.01] = 0.0

        # re-normalize
        nextrowsum = np.sum(pval[i - 1, :])
        pval[i - 1, :] = pval[i - 1, :] / nextrowsum

    # filter unreachable points and tuples out
    filterdbundles, filterpval = find_reachable(tracedbundles, pval)
    return filterdbundles, filterpval


def find_reachable(tracedbundles, pval):
    """
    Filter points and tuples out, which are ending in unreachable points.

    tracedbundles -- contribution matrix after traceback
    pval -- point values in traceback matrix

    Returns filtered contribution and traceback matrix.
    """
    filterdbundles = np.empty_like(tracedbundles, dtype=np.ndarray)
    filterpval = np.zeros_like(pval, dtype=float)
    for (i, j), bundles in np.ndenumerate(tracedbundles):
        # no bundle do nothing
        if bundles is None:
            continue
        # no active point do nothing
        if pval[i, j] == 0:
            continue
        reachable = []
        for d, v, o in bundles:
            # point (0, 0)
            if d == -2:
                reachable.append((d, v, o))
            elif d > -1:
                # is endpoint active too?
                if pval[i - 1, j - d] > 0:
                    reachable.append((d, v, o))
            else:
                assert False, "wrong direction while filtering?!"
        if len(reachable) == 0:
            # if no point is reachable, deactivate point
            pval[i, j] = 0
            filterdbundles[i, j] = None
        else:
            filterpval[i, j] = pval[i, j]
            filterdbundles[i, j] = reachable
    return filterdbundles, filterpval
