#!/usr/bin/env python3
"""
Probabilistic alignment of flowgrams against DNA sequences.
"""
import numpy as np
import numpy.ma as ma
import os
from matplotlib import pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from . pgrid import Svg, svg_text_and_grid, svg_lines, Line, Circle
from . pplot import plot_grid
from . pscoring import pscoring
from . ptraceback import trace_opacities


class FlowgramAlignment:
    # Reused from flowg

    """
    An alignment of a flowgram to a string.
    """

    def __init__(self, flows, seq, lengths):
        """
        flows --
        list that contains flows as (character, float) tuples and None values.
        seq --
        list of the strings of the reference that the flows are aligned to.
        lengths -- actual length of each flow (determined by alignment)
        """
        assert len(flows) == len(seq) == len(lengths)
        self.flows = flows
        self.seq = seq
        self.lengths = lengths

    def basecall(self):
        """
        Return the basecalled sequence, using the information in the flowgram
        """
        result = bytearray()
        for flow, length in zip(self.flows, self.lengths):
            if flow is None:
                # a deletion
                continue
            ch, _ = flow
            result.extend([ch] * length)
        return result


def pflowalign(flowgram, reference, plftau, weights, ml, strategy, plot,
               debug, svg, ca):
    """
    Possible alignments of a flowgram to a reference sequence.

    flowgram -- Flowgram object with flow chars and flow intensities
    reference -- reference sequence (bytes)
    plftau -- plf dictionary (P(f,l))
    weights -- dictionary of weights (w(l, shape))
    ml -- maximum homopolymer length (default: 15)
    strategy -- strategy of picking homopolymer length in the alignment step
    plot -- plotting enabled (True, False)
    debug -- debug enabled (True, False)
    svg -- SVG output enabled (True, False)
    ca -- comparison alignment of flowg
    """
    fchars, fvalues = flowgram.flowchars, flowgram.intensities
    assert len(fchars) == len(fvalues), "cannot mach flows with values"
    m = len(flowgram)
    n = len(reference)
    ref = reference.upper()
    # file name suffix
    fstr = "".join(chr(ch) + str(intensity)
                   for ch, intensity in flowgram)[:8]
    fpre = "".join([str(n), str(ref), fstr])
    # output directory
    filepath = "".join((os.path.dirname(__file__)[:-7], "output/"))

    """Scoring"""
    dbundles, scores, lbvalues, lvals_all = pscoring(
        ref, fchars, fvalues, plftau, weights, ml, filepath, fpre, debug)

    """Traceback and compute opacities"""
    filterdbundles, filterpval = trace_opacities(dbundles)

    if plot:
    # plot pcolormesh of masked scores
        possiblescores = ma.array(scores, mask=np.logical_not(filterpval))
        pp = PdfPages(filepath + "scores_allma_{}.pdf".format(fpre))
        plots = plot_grid(possiblescores[::-1],
                          "Scores of all possible waypoints.",
                          "Position j",
                          "Position i",
                          "all")
        plots.savefig(pp, format="pdf")
        plt.close("all")
        pp.close()

    alltuple = convert_matrix(list(), filterdbundles)
    """SVG output all, after traceback"""
    if svg:
        # init svg document, labels and circle grid
        # default scaling factor 100
        scale = 250
        doc_height = int((m + 1.5) * scale)
        doc_width = int((n + 1.5) * scale)
        document = Svg(doc_width, doc_height)
        svg_lines(document, alltuple, scale)
        svg_text_and_grid(document, m, n, fchars, fvalues, ref, scale)
        r = int(scale / 20)
        for (i, j), val in np.ndenumerate(filterpval):
            if filterpval[i, j] > 0:
                document.store(
                    Circle((j + 1) * scale,
                           (i + 1) * scale, r, "green", "green", 1))

        # alignment grid with all high possible paths as line objects
        document.save_svg(filepath + "svg{}_all".format(fpre))

    """Alignment - max path"""
    maxdbundles = np.empty_like(filterdbundles, dtype=np.ndarray)
    # filter max value out of each point (i, j)
    for (i, j), dbundles in np.ndenumerate(filterdbundles):
        if dbundles is None:
            continue
        else:
            maxdbundles[i, j] = [filterdbundles[i, j][0]]

    # get dict with connected waypoints
    maxwaypoints = connected_waypoints(maxdbundles)
    # only one connected path present
    maxpath = produce_paths(maxwaypoints, (m, n), (0, 0))
    # make the FlowgramAlignment object, and the list of the used ways
    maxalignment = next(convert_path_to_alignment(maxpath,
                                                  maxdbundles,
                                                  ref,
                                                  fchars,
                                                  fvalues,
                                                  strategy,
                                                  lbvalues,
                                                  lvals_all))
    yield maxalignment

    """Comparison SVG output flowg and max path"""
    if svg:
        """SVG output compare"""
        # remove lines and circles, to reuse document
        document.remove(Line)
        document.remove(Circle)

        if ca is not None:
            # path of flowg alignment
            flowgpath = convert_alignment_to_tuples(ca)
            # draw path of flowg alignment
            svg_lines(document, flowgpath, scale, 'red', 4)
        # draw path of pflowsa alignment
        svg_lines(document, maxalignment[1], scale, 'blue', 4)
        for (i, j), val in np.ndenumerate(filterpval):
            if filterpval[i, j] > 0:
                document.store(
                    Circle((j + 1) * scale,
                           (i + 1) * scale, r, "green", "green", 1))
        document.save_svg(filepath + "svg{}_bestcompare".format(fpre))

    """Alignment - high possible paths"""
    # get dict with connected waypoints
    hipconnectedpoints = connected_waypoints(filterdbundles)
    # produce a connected path
    hippath = produce_paths(hipconnectedpoints, (m, n), (0, 0))
    # make a FlowgramAlignment object, and a list of the used ways
    alignments = convert_path_to_alignment(hippath,
                                           filterdbundles,
                                           ref,
                                           fchars,
                                           fvalues,
                                           strategy,
                                           lbvalues,
                                           lvals_all)

    yield from alignments


def produce_paths(connectedwaypoints, startpoint, endpoint, visited=[]):
    """
    Finds all paths from a given set of connected
    waypoints.

    connectedwaypoints -- dictionary with connected waypoints
    startpoint -- point to start
    endpoint -- point to end
    visited -- visited points until now
    """
    visited = visited + [startpoint]
    # found a path yield it
    if startpoint == endpoint:
        yield visited
    # no connected path from start to end positon
    if startpoint not in connectedwaypoints.keys():
        assert False, "found no path?!"
    for nextwaypoint in connectedwaypoints[startpoint]:
        if nextwaypoint not in visited:
            yield from produce_paths(
                connectedwaypoints,
                nextwaypoint,
                endpoint,
                visited)


def convert_alignment_to_tuples(fla, dbundlesdict=None):
    """
    Makes a list of tuples (for the SVG output) out a FlowgramAlignment object.

    fla -- FlowgramAlignment object
    """
    i, j = 0, 0
    tuples = []
    for flow, length, seq in zip(fla.flows, fla.lengths, fla.seq):
        if flow is None:
            # a deletion go left (only in flowg alignments)
            j += 1
            tuples.append((-1, 0.7, i, j, 0.7))
            continue
        direction = len(seq)
        i += 1
        j += direction
        if dbundlesdict is None:
            value = 0.7
            opacity = 0.7
        else:
            _, value, _, _, opacity = dbundlesdict[i, j, direction]
        tuples.append((direction, value, i, j, opacity))
    return tuples


def convert_path_to_alignment(paths, dbundles, reference, fchars, fvalues,
                              strategy, lvals, lvals_all):
    """
    FlowgramAlignment of one or more paths,
    out of a given list of ways.

    paths -- a path through the graph
    dbundles -- dbundles matrix
    reference -- reference sequence
    fchars -- flow characters
    fvalues -- flow intensities
    strategy -- strategy of picking homopolymer length (d, dwithl, dwithlall)
    lvals -- best homopolymer lengths of one direction (i,j,d)
    lvals_all -- best homopolymer lengths of all directions (i,j)

    Returns FlowgramAlignment, and tuple list for SVG output.
    """
    for path in paths:
        ref = reference
        # aligned flows
        aflows = []
        # aligned sub sequences
        aseqs = []
        # aligned homopolymer lengths
        alengths = []
        # start at point down right
        inow, jnow = path[0]
        for point in path[1:]:
            inext, jnext = point
            direction = jnow - jnext
            if direction > -1:
                aflows.append((fchars[inow - 1], fvalues[inow - 1]))
                aseqs.append(ref[jnow - direction:jnow])
                # three alternatives to choose homopolymer length
                if strategy == 'd':
                    # direction d as length
                    alengths.append(direction)
                if strategy == 'dwithl':
                    # best length l in d
                    alengths.append(lvals[inow, jnow, direction])
                if strategy == 'dwithlall':
                    # best length in all ds
                    alengths.append(lvals_all[inow, jnow])

            else:
                assert False, "wrong direction while path converting?!"
            inow, jnow = inext, jnext
        aflows.reverse()
        aseqs.reverse()
        alengths.reverse()
        alignment = FlowgramAlignment(aflows, aseqs, alengths)
        # make a list of tuples to draw as svg
        dbundlesdict = convert_matrix(dict(), dbundles)
        tuples = convert_alignment_to_tuples(alignment, dbundlesdict)
        yield alignment, tuples


def connected_waypoints(dbundles):
    """
    Computes a dictionary with a waypoint as key, containing a list
    of its succesors as value.

    dbundles -- all directional bundles

    Returns a dictionary (inow, jnow): [(inext, jnext), (i2next, j2next), ...].
    """
    connectedpoints = {}
    cpkeys = connectedpoints.keys()
    m, n = dbundles.shape
    for i in range(m - 1, -1, -1):
        for j in range(n - 1, -1, -1):
            bundles = dbundles[i, j]
            if bundles is None:
                continue
            for direction, value, opacity in bundles:
                if direction == -2:
                    connectedpoints[i, j] = [(i, j)]
                elif direction > -1:
                    if (i, j) in cpkeys:
                        current = connectedpoints[i, j]
                        new = current + [(i - 1, j - direction)]
                        connectedpoints[i, j] = new
                    else:
                        connectedpoints[i, j] = [(i - 1, j - direction)]
    return connectedpoints


def convert_matrix(totype, dbundles):
    """
    Converts a matrix to given type (list or dict) containing all
    usable ways, to be drawn in the svg document.

    totype -- specific type
    dbundles -- dbundles matrix

    Returns the totype object.
    """
    if isinstance(totype, dict):
        dbundlestotype = {}
    elif isinstance(totype, list):
        dbundlestotype = []
    else:
        assert False, "convert type '{type}' not handled yet ;(".format(
            type=totype)

    for (i, j), bundles in np.ndenumerate(dbundles):
        if bundles is None:
            continue
        for direction, dbvalue, opacity in bundles:
            if isinstance(totype, dict):
                dbundlestotype[i, j, direction] = (direction,
                                                   dbvalue, i, j, opacity)
            if isinstance(totype, list):
                dbundlestotype.append((direction, dbvalue, i, j, opacity))
    return dbundlestotype
