#!/usr/bin/env python3
"""
Components to create an .svg file containing an alignment grid.
"""


class Svg:

    """
    SVG Document
    """

    def __init__(self, width, height):
        """
        width -- width of document
        height -- height of document
        """
        self.width = width
        self.height = height
        self.elements = []
        return

    def features(self):
        # set SVG document properties
        # xmlns means to use of the default namespace
        lines = ["<?xml version=\"1.0\"?>\n",
                 "<svg width=\"{}\" height=\"{}\" viewBox=\"0 0 {} {}\"\n"
                 .format(
                     self.width,
                     self.height,
                     self.width,
                     self.height),
                 "     xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">",
                 "\n"]
        for e in self.elements:
            lines += e.features()
        lines += ["</svg>\n"]
        return lines

    def store(self, element):
        # attach object to document
        if isinstance(element, Line):
            # store the lines before pattern to place them in the background
            self.elements.insert(len(self.elements) - 1, element)
        else:
            self.elements.append(element)

    def save_svg(self, filename):
        # save document with attached objects
        self.filename = filename + ".svg"
        with open(self.filename, 'w') as f:
            f.writelines(self.features())
        return

    def remove(self, etype):
        # remove all objects of a given element type
        self.elements = [x for x in self.elements if not isinstance(x, etype)]


class Circle:

    """
    SVG Circle
    """

    def __init__(self, cx, cy, r, fill, stroke, stroke_width):
        """
        cx -- x coordinate
        cy -- y coordinate
        r -- radius
        fill -- fill color
        stroke -- stroke color
        stroke_width -- thickness of stroke
        """
        self.cx = cx
        self.cy = cy
        self.r = r
        self.fill = fill
        self.stroke = stroke
        self.stroke_width = stroke_width
        return

    def features(self):
        # set circle properties
        return ["  <circle cx=\"{}\" cy=\"{}\" r=\"{}\"\n\
            fill=\"{}\" stroke=\"{}\" stroke-width=\"{}\"  />\n".format(
            self.cx,
            self.cy,
            self.r,
            self.fill,
            self.stroke,
            self.stroke_width)]


class Pattern:

    """
    SVG Pattern
    """

    def __init__(self, identifier, x, y, width, height,
                 element, doc_width, doc_height):
        """
        identifier -- pattern identifier
        x -- x coordinate
        y -- y coordinate
        width -- width of pattern
        height -- height of pattern
        element -- SVG element for the pattern
        doc_width -- width of the used SVG document
        doc_height -- height of the used SVG document
        """
        self.id = identifier
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.element = element
        self.doc_width = doc_width
        self.doc_height = doc_height
        return

    def features(self):
        # set pattern properties
        lines = ["  <pattern id=\"{}\" x=\"{}\" y=\"{}\" ".format(
                 self.id,
                 self.x,
                 self.y),
                 "width=\"{}\" height=\"{}\" \n".format(
                     self.width,
                     self.height),
                 "           patternUnits=\"userSpaceOnUse\">\n  "]
        lines += self.element.features()
        lines += ["  </pattern>\n",
                  "  <rect x=\"{}\" y=\"{}\" height=\"{}\" width=\"{}\" \n\
        style=\"fill:url(#{});\" />\n".format(
                      (self.width - 100),
                      (self.height - 100),
                      self.doc_height - 100,
                      self.doc_width - 100,
                      self.id)]
        return lines


class Text:

    """
    SVG Text
    """

    def __init__(self, x, y, font_size, fill, char, flowvalue=None):
        """
        x -- x coordinate
        y -- y coordinate
        font_size -- font size given in pixel
        fill -- fill color
        char -- flow character or reference character
        flowvalue -- intensity of flow character
        """
        self.x = x
        self.y = y
        self.font_size = font_size
        self.fill = fill
        self.char = char
        self.flowvalue = flowvalue
        return

    def features(self):
        # set text properties
        lines = ["  <text x=\"{}\" y=\"{}\"\n\
        font-size=\"{}\" fill=\"{}\" >\n"
                 .format(
                     self.x,
                     self.y,
                     self.font_size,
                     self.fill),
                 "    {}\n".format(self.char)]

        if self.flowvalue is not None:
            lines += [
                "    <tspan baseline-shift=\"super\" font-size=\"{}\">\n".
                format(
                    self.font_size),
                "      {}\n".format(self.flowvalue),
                "    </tspan>\n"]

        lines += ["  </text>\n"]
        return lines


class Line:

    """
    SVG Line
    """

    def __init__(self, x1, y1, x2, y2, stroke, stroke_width, stroke_opacity):
        """
        x1 -- x coordinate (start)
        x2 -- x coordinate (end)
        y1 -- y coordinate (start)
        y2 -- y coordinate (end)
        stroke -- stroke color
        stroke_width -- thickness of stroke
        stroke_opacity -- transparency of stroke (float value between 0 and 1)
        """
        self.x1 = x1
        self.x2 = x2
        self.y1 = y1
        self.y2 = y2
        self.stroke = stroke
        self.stroke_width = stroke_width
        self.stroke_opacity = stroke_opacity
        return

    def features(self):
        # set line properties
        return ["  <line x1=\"{}\" y1=\"{}\" x2=\"{}\" y2=\"{}\"\n\
        stroke=\"{}\" stroke-width=\"{}\"\n\
        stroke-opacity=\"{}\"  />\n".format(
                self.x1,
                self.y1,
                self.x2,
                self.y2,
                self.stroke,
                self.stroke_width,
                self.stroke_opacity)]


def svg_text_and_grid(document, m, n, fchars, fvalues, ref, scale=250):
    """
    Attach circle grid, reference and flowgram chars to a given SVG document.

    document -- SVG document to manipulate
    m, n -- height and width of grid used in SVG document
    fchars -- flow characters
    fvalues -- intensities of flow characters
    ref -- chars of reference
    scale -- scaling factor (default 250)
    """
    # flows of flowgram top down
    for i in range(m):
        document.store(
            Text(
                int(scale / 4),
                i * scale + 1.6 * scale,
                int(scale / 5),
                "black",
                chr(fchars[i]),
                str(fvalues[i])))

    # reference characters left to right
    for j in range(n):
        document.store(
            Text(
                j * scale + 1.4 * scale,
                int(scale / 2),
                int(scale / 5),
                "black",
                chr(ref[j])))

    # grid of circles using a pattern,
    # pattern are not as accurate as circles
    # would be better to draw circle by circle,
    # but then zooming is not possible
    r = int(scale / 20)
    pattern_element = Circle(r * 2, r * 2, r, "whitesmoke", "whitesmoke", 1)
    document.store(
        Pattern(
            "circle_pattern", -r * 2, -r * 2, scale, scale, pattern_element,
            document.width, document.height))


def svg_lines(document, dbundles, scale=250, color='black', magic=None):
    """
    Attaches lines to a given SVG document.

    document -- svg canvas
    dbways -- all ways which will be drawn
    scale -- scaling factor (default 250)
    color -- color of lines
    magic -- overwrite opacities by given number (testing purpose)
    """
    # begin at bottom right corner
    for bundle in dbundles[::-1]:
        # direction, bundle value, position i & j, opacity
        d, v, i, j, o = bundle
        # overwrite opacity values by a magic value, to show all ways clearer
        if magic is not None:
            o = magic
        # positions + 1 because of labels in document
        inow = i + 1
        jnow = j + 1
        if d == -2:
            continue
        elif d == -1:
            inext = inow
            jnext = jnow - 1
        elif d > -1:
            inext = inow - 1
            jnext = jnow - d
        else:
            assert False, "wrong direction"
        document.store(
            Line(
                jnow * scale,
                inow * scale,
                jnext * scale,
                inext * scale,
                color,
                o,
                o))
