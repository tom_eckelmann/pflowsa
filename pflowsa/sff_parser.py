from Bio import SeqIO
"""
Naive SFF Parser
"""


def main():
    flows = []
    a = []
    bases = list('TACG' * 200)
    for record in SeqIO.parse("flows.sff", "sff"):
        a.append(record)
        flow_values_raw = record.annotations["flow_values"]
        flow_values = [str(x/100) for x in flow_values_raw]
        base_value_tuple_list = [''.join(x) for x in zip(bases, flow_values)]
        flows.append(' '.join(base_value_tuple_list))

    print((a[0].seq[:]), flows[0])

    # print(a[0].annotations["flow_values"])
    # print(a[1].annotations["flow_values"])
    # print(a.annotations['flow_index'])
    # print(a[0])
    # print(a[1])

if __name__ == '__main__':
    main()
