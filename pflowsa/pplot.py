#!/usr/bin/env python3
"""
Plotting.
"""
import numpy as np
from matplotlib import pyplot as plt
from itertools import chain


def plot_grid(grid, title, xlabel, ylabel, whattoplot=""):
    """
    Plot a grid as pcolormesh.

    grid -- two dimensional grid
    title -- grid title
    xlabel -- grid label for x-axis
    ylabel -- grid label for y-axis
    whattoplot -- defines type ("all" = endscores, position scores otherwise)

    (Labels and values are only printed if grid does not exceed size.)

    Returns a Figure object.
    """
    title = title
    xlabel = xlabel
    ylabel = ylabel

    fig = plt.figure()
    plt.pcolormesh(grid, cmap=plt.cm.Reds)
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    ymax, xmax = grid.shape
    plt.ylim((0, ymax))
    plt.xlim((0, xmax))

    # only draw labels if not more than 25 values [0...24]
    if ymax < 25 or xmax < 25:
        x = range(xmax)
        # if "all" print y label in reversed order
        if whattoplot == "all":
            y = range(ymax - 1, -1, -1)
        else:
            y = range(ymax)
        indx = np.arange(0.5, xmax, 1)
        indy = np.arange(0.5, ymax, 1)
        plt.xticks(indx, x)
        plt.yticks(indy, y)
        for (i, j), value in np.ndenumerate(grid):
            if value > 0:
                plt.text(
                    j + .5,
                    i + .5,
                    "{:0.4f}".format(value),
                    size=7,
                    ha="center",
                    va="center"
                )
        labels = "on"
    else:
        labels = "off"
    plt.tick_params(
        axis="x",
        which="both",
        bottom="off",
        top="off",
        labelbottom=labels)
    plt.tick_params(
        axis="y",
        which="both",
        left="off",
        right="off",
        labelleft=labels)
    return fig


def plot_compare_bars(datapflowsa, dataround, dataflowg):
    """
    Plot number of edit distances of sequences from flowg,
    pflowsa and base called sequence to true mutated simulated fasta sequence.

    datapflowsa -- edit distances of pflowsa
    dataround -- edit distances of base calling
    dataflowg -- edit distances of flowg

    Returns a Figure object.
    """

    pos = np.arange(len(datapflowsa))
    width = 0.3
    fig = plt.figure()
    ax = fig.add_subplot(111)

    barsround = ax.bar(pos, dataround, width, color="yellow")
    barsflowg = ax.bar(pos + width, dataflowg, width, color="red")
    barspflowsa = ax.bar(pos + 2 * width, datapflowsa, width, color="blue")

    ax.legend((barsround[0], barsflowg[0], barspflowsa[0]),
              ("base calling", "flowg", "pflowsa"))
    ax.set_xlim(-width, len(pos) + width)
    ax.set_ylim(0, 900)
    ax.set_ylabel('Anzahl')
    ax.set_xlabel('Edit-Distanz')
    ax.set_xticks(pos + width + .15)
    ax.set_xticklabels([i for i in range(len(dataflowg))])

    for bar in chain(barsflowg, barsround, barspflowsa):
        height = bar.get_height()
        ax.text(bar.get_x() + bar.get_width() / 2, height + 10,
                '%d' % int(height), ha='center', va='bottom', size=15,
                rotation="90")

    return plt
