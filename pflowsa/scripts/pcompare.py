#!/usr/bin/env python3

import os
import sys
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from collections import Counter
from .. pplot import plot_compare_bars
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib import pyplot as plt


class HelpfulArgumentParser(ArgumentParser):
    # Reused from flowg

    """An ArgumentParser that prints full help on errors."""

    def error(self, message):
        self.print_help(sys.stderr)
        args = {'prog': self.prog, 'message': message}
        self.exit(2, '%(prog)s: error: %(message)s\n' % args)


def get_argument_parser():
    p = HelpfulArgumentParser(description=__doc__,
                              formatter_class=RawDescriptionHelpFormatter)
    p.add_argument("flowg", nargs='?',
                   help="Data of flowg edit distances (.edisto).")
    p.add_argument("pflowsa", nargs='?',
                   help="Data of pflowsa edit distances (.edisto).")
    p.add_argument("file", nargs='?',
                   help="Output filename (bars.pdf).")
    return p


def compare(flowgfile, pflowsafile, filename):
    filepath = "".join((os.path.dirname(__file__)[:-15], "pipeline/"))

    with open(filepath + flowgfile, mode="r", encoding="utf8") as d:
        data = d.read().splitlines()
    dataround = []
    dataflowg = []
    maxa = 0
    maxb = 0
    for line in data:
        a, b = line.split()
        a = int(a)
        b = int(b)
        dataround.append(a)
        dataflowg.append(b)
        maxa = a if a > maxa else maxa
        maxb = b if b > maxb else maxb
    with open(filepath + pflowsafile, mode="r", encoding="utf8") as d:
        data = d.read().splitlines()

    datapflowsa = []
    maxc = 0
    for line in data:
        _, c = line.split()
        c = int(c)
        datapflowsa.append(c)
        maxc = c if c > maxc else maxc

    countround = Counter(dataround)
    countflowg = Counter(dataflowg)
    countpflowsa = Counter(datapflowsa)

    length = max((maxa, maxb, maxc)) + 1
    dataround = [0 for i in range(length)]
    dataflowg = [0 for i in range(length)]
    datapflowsa = [0 for i in range(length)]
    for count in countround:
        dataround[count] = countround[count]
    for count in countflowg:
        dataflowg[count] = countflowg[count]
    for count in countpflowsa:
        datapflowsa[count] = countpflowsa[count]
    # print(dataround)
    # print(dataflowg)
    # print(datapflowsa)
    plot = plot_compare_bars(datapflowsa, dataround, dataflowg)

    pp = PdfPages(filepath+filename)
    plot.savefig(pp, format="pdf")
    plt.close("all")
    pp.close()


def main():
    parser = get_argument_parser()
    args = parser.parse_args()
    compare(args.flowg, args.pflowsa, args.file)


if __name__ == '__main__':
    main()
