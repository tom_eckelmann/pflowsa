#!/usr/bin/env python3
"""
Probabilistic flowgram string alignment.

Give a reference and a flowgram as command-line parameters,
or read them from an input file.

bin/pflowsa TCCCG "T1.2 A0.1 C3.6 G1.0"
bin/pflowsa -i tryme.txt
"""
import sys
import os
from argparse import ArgumentParser, RawDescriptionHelpFormatter, FileType
from collections import Counter
from glob import glob
from .. flowgram import parse_example, parse_flowgram
from .. palign import pflowalign
from .. pdicts import plftau_dict, weights_dict

# flowg modules for comparison
from .. align import flowalign
from .. scoring import get_gcb_scoring


class HelpfulArgumentParser(ArgumentParser):
    # Reused from flowg

    """An ArgumentParser that prints full help on errors."""

    def error(self, message):
        self.print_help(sys.stderr)
        args = {'prog': self.prog, 'message': message}
        self.exit(2, '%(prog)s: error: %(message)s\n' % args)


def process(reference, flowgram, plftau, weights, ml, strategy, sc, asteps,
            plot, debug, svg):
    """
    Process the alignments.

    reference -- reference sequence (bytes)
    flowgram -- Flowgram object with flow chars and flow intensities
    plftau -- plf dictionary (P(f,l))
    weights -- dictionary of weights (w(l, shape))
    ml -- maximum homopolymer length (default: 15)
    strategy -- strategy of picking homopolymer length in the alignment step
    sc -- scoring for flowg
    asteps -- print out alignment steps of pflowsa-max alignment (True, False)
    plot -- plotting enabled (True, False)
    debug -- debug enabled (True, False)
    svg -- SVG output enabled (True, False)

    Prints alignment sequences.
    """

    scoring = sc
    # comparison alignment of flowg
    calignment = flowalign(flowgram, reference, scoring)
    # flowg sequence
    fg = calignment.basecall().decode()
    # base called sequence
    bc = ''.join(int(intensity + 0.5) * chr(ch) for ch, intensity in flowgram)

    print('reference:  ', reference.decode())
    print('base-called:', bc)
    print('flowg-read: ', fg)

    alignment = pflowalign(
        flowgram,
        reference,
        plftau,
        weights,
        ml,
        strategy,
        plot,
        debug,
        svg,
        calignment)
    # pflowsa sequence(s)
    seen = []
    i = -1
    try:
        for algn in alignment:
            # restriction to max 1000 alignment sequences
            if i > 999:
                print("more than 1000, break")
                break
            a, _ = algn
            astr = a.basecall().decode()
            if i == -1:
                print('pflowsa-max:', astr)
                if asteps:
                    # output of all alignment steps for pflowsa-max
                    flows = ["".join((chr(ch), str(f))) for ch, f in a.flows]
                    seqs = [seq.decode() for seq in a.seq]
                    print([x for x in zip(seqs, flows, a.lengths)])
            else:
                seen.append(astr)
            i += 1
    except KeyboardInterrupt:
        # show number of paths until now
        print(i)
    c = Counter(seen)
    for seq in c:
        print("{seq}:\t{count}".format(seq=seq, count=c[seq]))
    print()


def get_argument_parser():
    p = HelpfulArgumentParser(description=__doc__,
                              formatter_class=RawDescriptionHelpFormatter)
    p.add_argument("-i", "--input", type=FileType('r'),
                   help="Read in a file with multiple \
                   reference/flowgram pairs (one per line).")
    p.add_argument("reference", nargs='?',
                   help="The reference string, such TCGGG")
    p.add_argument("flowgram", nargs='?',
                   help='A flowgram, such as "T1.2 A0.1 C0.5 G3.6" \
                    (you may need to add the quote characters)')
    p.add_argument("-nu", "--nu", type=float,
                   default=0.001, help="Value of nu (indel).")
    p.add_argument("-pn", "--pn", type=float, default=0.003,
                   help="Value of p_n (mismatch).")
    p.add_argument("-ml", "--maxlength", type=int, default=15,
                   help="Max length of single homopolymer.")
    p.add_argument("-p", "--plot", action='store_true',
                   help="Draw a pcolormesh / heatmap \
                   with the computet scores.")
    p.add_argument("-svg", "--svggrids", action='store_true',
                   help="Draw SVG grids of alignments.")
    p.add_argument("-d", "--debug", action='store_true',
                   help="Only for debugging purpose and short references.")
    p.add_argument("-a", "--alignmentsteps", action='store_true',
                   help="Print alignmentsteps for pflowsa-max alignment.")
    p.add_argument("-deld", "--deletedicts", action='store_true',
                   help="Delete all created pickle (.p) files containing the \
                   P(f,l) dictionary or the W(l,shape) dictionaries \
                   from the used -s -n -ml combinations.")
    p.add_argument("-s", "--strategy", type=str,
                   choices=['d', 'dwithl', 'dwithlall'],
                   help="Which option to choose for alignment step \
                   homopolymer length. Three different choices: \
                   'd' for direction only, 'dwithl' for direction and best \
                   length out of this direction or 'dwithlall' for direction \
                   and best length of all directions.", default='dwithl')
    return p


def main():
    parser = get_argument_parser()
    args = parser.parse_args()

    # error outputs
    if args.input is None and args.reference is None:
        parser.error("Need an input file or a reference and flowgram.")
    if args.reference is not None and args.flowgram is None:
        parser.error("Both reference and flowgram must be given.")
    if args.nu is not None and args.nu < 0:
        parser.error("Please use a positive value or zero for nu.")
    if args.pn is not None and args.pn < 0:
        parser.error("Please use a positive value or zero for P_n.")
    if args.maxlength < 15:
        parser.error("Please use a value of 15 or greater for the maxlength.")
    # delete the pickled dicts
    if args.deletedicts:
        files = "".join((os.path.dirname(os.path.dirname(__file__)),
                         "/data/*.p"))
        dictfiles = glob(files)
        for dictfile in dictfiles:
            os.unlink(dictfile)

    # compute plftau and weights
    plftau, _, _ = plftau_dict()
    weights = weights_dict(args.pn,
                           args.nu,
                           args.maxlength)

    # scoring of flowg for comparison
    sc = get_gcb_scoring(True)

    # direct input
    if args.reference is not None:
        flowgram = parse_flowgram(args.flowgram)
        reference = args.reference.encode()
        process(reference,
                flowgram,
                plftau,
                weights,
                args.maxlength,
                args.strategy,
                sc,
                args.alignmentsteps,
                args.plot,
                args.debug,
                args.svggrids)

    # file input
    if args.input:
        for line in args.input:
            line = line.strip()
            if len(line) == 0 or line.startswith('#'):
                continue
            reference, flowgram, comment = parse_example(line)
            process(reference,
                    flowgram,
                    plftau,
                    weights,
                    args.maxlength,
                    args.strategy,
                    sc,
                    args.alignmentsteps,
                    args.plot,
                    args.debug,
                    args.svggrids)

if __name__ == '__main__':
    main()
