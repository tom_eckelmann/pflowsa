#!/usr/bin/env python3
"""
Two dictionaries, which are needed for the scoring algorithm.
"""
import numpy as np
import os
import pickle
from collections import defaultdict
from math import fsum


def plftau_dict():
    """
    Computes (sequencing error) probabilities of (l, f) pairings.

    Returns plftau, plf, tau (defaultdicts).
    """
    filepathraw = "".join((os.path.dirname(__file__), "/data/plfstats.txt"))
    filepathpickle = "".join((os.path.dirname(__file__), "/data/plfstats.p"))
    try:
        plftau, plf, tau = pickle.load(open(filepathpickle, "rb"))
    except:
        try:
            with open(filepathraw,
                      mode="r",
                      encoding="utf8") as d:
                data = d.read().splitlines()
                pairings = set()
                for line in data:
                    # homopolymer length l | intensity f | occurencies n
                    l, f, n = line.split()
                    pairings.add((int(l), float(f), int(n)))
                # sum of all occurences is 777897804
                nsum = fsum(n for _, _, n in pairings)
                # all measured different intensities
                fkeys = list(set(f for _, f, _ in pairings))
                # ratio, (l,f): number of occurence / 777897804
                plf = defaultdict(
                    float, {(l, f): float(n / nsum) for l, f, n in pairings})
                # number of aligning an given
                # intensity in the whole data, f: plf(*, f)
                tau = defaultdict(
                    float,
                    {f: fsum(plf[k] for k in plf.keys() if k[1] == f)
                        for f in fkeys})

                # maybe used later
                # lkeys = list(set(l for l, _, _ in values))
                # pi = defaultdict(
                #     float,
                #     {l: fsum(plf[k] for k in plf.keys() if k[0] == l)
                #         for l in lkeys})

                # (l, f): plf(l,f) / tau(f)
                plftau = defaultdict(
                    float,
                    {(l, f): float(v / tau[f]) for (l, f), v in plf.items()})
                # assert len(plf) == len(plftau)  # 15635 items
                pickle.dump((plftau, plf, tau), open(filepathpickle, "wb"))
                # returning of plf and tau only for testing purpose
                return plftau, plf, tau
        except (OSError, IOError) as e:
            print("Compute P(f,l) failed.")
            exit(e)
    return plftau, plf, tau


def compute_ps(pn=0.003, nu=0.001):
    """Returns ps from given probabilities."""
    return 4 - 8 * nu - 3 * pn


def weights_dict(pn=0.003, nu=0.001, maxl=15):
    """
    Computes for all possible combinations of shapes and homopolymer lengths
    the (sequence editing) weights.

    nu -- given pvalue for nu (indel operations)
    pn -- given value for P_n (mismatch operation)

    Returns weights (defaultdict).
    """
    filename = "/data/weights_{maxl}_{nu}_{pn}.p".format(
        maxl=maxl,
        nu=str(nu).replace(".", ""),
        pn=str(pn).replace(".", ""))
    filepathpickle = "".join((os.path.dirname(__file__), filename))
    try:
        weights = pickle.load(open(filepathpickle, "rb"))
    except:
        pn = pn
        nu = nu
        kappa = 15
        ps = compute_ps(pn, nu)
        # default ps = 3.9
        weights = defaultdict(float, {})
        grid = np.zeros((maxl, kappa), dtype=float)  # default 15 x 15 matrix
        # first row values for empty flow
        grid[0, :] = [nu**i for i in range(kappa)]
        # entry mass has to be 1.0
        assert grid[0, 0] == 1.0
        weights[0, ''] = 1.0
        # first column values for empty shape
        grid[:, 0] = [nu**i for i in range(maxl)]

        # all possible (rep)resentatives
        reps = set()
        for l in range(1, kappa):
            for rep in ('1' * i + '0' * (l - i) for i in range(l + 1)):
                reps.add(rep)
            weights[0, '0' * l] = grid[0, l]
            weights[l, ''] = grid[l, 0]

        # compute values
        for l in range(1, maxl):
            for rep in reps:
                replen = len(rep)
                # homopolymer length x rep length segment of grid (hl x sl)
                # compute values, override old ones
                for hl in range(1, l + 1):
                    for sl in range(1, replen + 1):
                        # 1 match, 0 mismatch probability
                        p = ps if rep[sl - 1] == '1' else pn
                        # sum of above left, above and left neighbor
                        grid[hl, sl] = (grid[hl - 1, sl - 1] * p) + \
                            ((grid[hl - 1, sl] + grid[hl, sl - 1]) * nu)
                # save last entry
                weights[hl, rep] = grid[hl, sl]
        pickle.dump(weights, open(filepathpickle, "wb"))
        return weights
    return weights
