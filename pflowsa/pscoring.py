#!/usr/bin/env python3
"""
Scoring function.
"""
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from . pplot import plot_grid


def pscoring(reference, fchars, fvalues, plftau, weights,
             ml, fpath, fname, debug=False):
    """
    Probabilistic scoring of flows to a reference.

    reference -- reference sequence (bytes)
    fchars -- flow characters
    fvalues -- flow intensities
    plftau -- plftau dictionary (P(f,l))
    weights -- dictionary of weights (w(l, shape))
    ml -- maximum homopolymer length (default: 15)
    fpath -- path to output directory
    fname -- file name suffix
    debug -- debug enabled (True, False)

    Returns the scoring, directional bundle and the best lengths matrices.
    """
    m = len(fchars)
    n = len(reference)
    ref = reference
    kappa = 15
    # every point has a score
    scores = np.zeros((m + 1, n + 1), dtype=float)
    #      chars of reference
    #       --------------->
    #     |        n
    # f   |
    # l   |
    # o   | m
    # w   |
    # s   |
    #     V

    # stores for every point a list of dbundle represented as tuple (d, sd)
    # a maximum of 15 dbundle are stored (only look 14 steps back + 1 up)
    # every tuple contains the direction d from where the bundle is from
    # and the value sd
    # d means:
    #             -2  start point, only present at (0, 0)
    #              0  comes from above (i - 1, j)
    #     0 < d < 15  comes from (i - 1, j - d)
    # sd is the contingent of dbundle to the score of (i, j) ([0.0,...,1.0])
    dbundles = np.zeros((m + 1, n + 1), dtype=np.ndarray)
    # best length contingent for a combination of i, j, d
    lbvalues = {}
    # best length of all directions
    lvals_all = {}
    # start at point (0, 0)
    scores[0, 0] = 1.0
    dbundles[0, 0] = [(-2, 1.0)]

    if debug:
        # init pdf to plot pcolormesh
        pp = PdfPages(fpath+"hmap_{fname}.pdf".format(fname=fname))
        # grid with all contigents of a dbundle
        grid = np.zeros((ml, ml), dtype=float)

    # list with contingents of dbundles to the score, which will be stored
    # in dbundles later
    dbscores = np.zeros(kappa, dtype=float)
    # length contingents for a bundle from a direction to a point
    lbvalue = np.zeros(ml, dtype=float)
    # length contingents of all directions
    lall = np.zeros(ml, dtype=float)
    # skip first row
    for i in range(1, m + 1):
        # flow char
        b = fchars[i - 1]
        # flow intensity
        f = fvalues[i - 1]
        for j in range(0, n + 1):
            # length of substring + 1 because of empty suffix
            dlen = j + 1
            # only 15 dbundles, max suffix length of 14 + empty suffix
            if dlen > kappa:
                dlen = kappa
            score = 0.0
            dbscores.fill(0.0)
            lall.fill(0.0)
            # score for each dbundle
            for d in range(dlen):
                lbvalue.fill(0.0)
                # if predecessor is zero
                if scores[i - 1][j - d] == 0:
                    continue
                # suffix of reference
                suffix = ref[j - d:j]
                matches = suffix.count(b)

                # l = 0
                # representative of suffix
                rep = "0" * d
                # item score (iscore) of dbundle
                iscore = weights[0, rep] * plftau[0, f] * \
                    scores[i - 1][j - d]
                # print(r, i, j, iscore)

                # score of bundle
                dbscore = iscore
                lbvalue[0] = iscore
                lall[0] += iscore
                if debug:
                    grid[d, 0] = iscore

                # l = 1,...,ml
                rep = "1" * matches + "0" * (d - matches)
                for l in range(1, ml):
                    iscore = weights[
                        l, rep] * plftau[l, f] * scores[i - 1][j - d]
                    dbscore += iscore
                    lbvalue[l] = iscore
                    lall[l] += iscore
                    if debug:
                        grid[d, l] = iscore
                score += dbscore
                # normalize l
                if dbscore > 0:
                    lbvalue = lbvalue / dbscore
                dbscores[d] = dbscore
                lbvalues[i, j, d] = lbvalue.argmax()
            scores[i, j] = score
            # normalize dbundle scores, best lengths of all directions
            if score > 0:
                dbscores = dbscores / score
                lall = lall / score
            lvals_all[i, j] = lall.argmax()
            # print(i, j, dbscores)
            dbs = []
            for d, dbscore in enumerate(dbscores):
                dbs.append((d, dbscore))
            dbundles[i, j] = dbs
            if debug:
                # d & l combination with highest contigent
                d, l = np.unravel_index(grid.argmax(), grid.shape)
                print("({i}, {j}) d: {d} l: {l}".format(i=i, j=j, d=d, l=l))

                # plot pcolormesh of l and t combination score pieces
                title = "Position {i}, {j}".format(i=str(i), j=str(j + 1))
                plots = plot_grid(grid, title,
                                  "Length of homopolymer",
                                  "Direction to predecessor")
                plots.savefig(pp, format="pdf")
                plt.close("all")
    if debug:
        # close pdf with l and d combination plots
        pp.close()
    return dbundles, scores, lbvalues, lvals_all
