#!/usr/bin/env python3

from pflowsa.pdicts import plftau_dict, weights_dict
from pflowsa.pscoring import pscoring
from pflowsa.flowgram import parse_flowgram


def test_pscoring():
    plftau, _, _ = plftau_dict()
    weights = weights_dict(pn=0.3, nu=0.1, maxl=15)
    ml = 15
    fpath = ""
    fname = ""
    debug = False
    flowgram = parse_flowgram("A0 C1.0 G2.0 T3.0")
    reference = "AAAAAA ".encode()
    fchars, fvalues = flowgram.flowchars, flowgram.intensities

    dbundles, scores, lbvalues, lallvals = pscoring(reference,
                                                    fchars,
                                                    fvalues,
                                                    plftau,
                                                    weights,
                                                    ml,
                                                    fpath,
                                                    fname,
                                                    debug)
    assert scores[0, 0] == 1
    assert dbundles[0, 0] == [(-2, 1.0)]
    assert scores.shape == dbundles.shape
    assert lbvalues[4, 7, 3] == 3
