#!/usr/bin/env python3

from pflowsa.pgrid import Svg, Line, Circle, Pattern, Text


def test_pgrid():
    # init
    document = Svg(200, 300)
    assert document.height == 300
    assert document.width == 200
    data = document.features()
    assert data == ['<?xml version="1.0"?>\n',
                    '<svg width="200" height="300" viewBox="0 0 200 300"\n',
                    '     xmlns="http://www.w3.org/2000/svg" version="1.1">',
                    '\n', '</svg>\n']
    # store and Circle features
    circle = Circle(400, 200, 3, "green", "green", 1)
    document.store(circle)
    data = document.features()
    assert data == ['<?xml version="1.0"?>\n',
                    '<svg width="200" height="300" viewBox="0 0 200 300"\n',
                    '     xmlns="http://www.w3.org/2000/svg" version="1.1">',
                    '\n',
                    '  <circle cx="400" cy="200" r="3"\n            fill="green" stroke="green" stroke-width="1"  />\n',
                    '</svg>\n']
    # remove
    document.remove(Circle)
    data = document.features()
    assert data == ['<?xml version="1.0"?>\n',
                    '<svg width="200" height="300" viewBox="0 0 200 300"\n',
                    '     xmlns="http://www.w3.org/2000/svg" version="1.1">',
                    '\n', '</svg>\n']

    line = Line(1, 2, 3, 'red', 5, 6, .5)
    assert line.features() == [
        '  <line x1="1" y1="2" x2="3" y2="red"\n        stroke="5" stroke-width="6"\n        stroke-opacity="0.5"  />\n']

    text = Text(4, 5, 20, 'black', 'C', None)
    flowtext = Text(2, 7, 15, 'blue', 'A', 2.3)
    assert text.features() == ['  <text x="4" y="5"\n        font-size="20" fill="black" >\n',
                               '    C\n',
                               '  </text>\n']
    assert flowtext.features() == ['  <text x="2" y="7"\n        font-size="15" fill="blue" >\n',
                                   '    A\n',
                                   '    <tspan baseline-shift="super" font-size="15">\n',
                                   '      2.3\n',
                                   '    </tspan>\n',
                                   '  </text>\n']

    pattern = Pattern(
        "circle_pattern", -4 * 2, -4 * 2, 50, 50, circle,
        400, 500)
    assert pattern.features() == ['  <pattern id="circle_pattern" x="-8" y="-8" ',
                                  'width="50" height="50" \n',
                                  '           patternUnits="userSpaceOnUse">\n  ',
                                  '  <circle cx="400" cy="200" r="3"\n            fill="green" stroke="green" stroke-width="1"  />\n',
                                  '  </pattern>\n',
                                  '  <rect x="-50" y="-50" height="400" width="300" \n        style="fill:url(#circle_pattern);" />\n']
