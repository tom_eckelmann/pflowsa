#!/usr/bin/env python3

from pflowsa.palign import pflowalign
from pflowsa.pdicts import plftau_dict, weights_dict
from pflowsa.flowgram import parse_flowgram


def test_pflowalign():
    plftau, _, _ = plftau_dict()
    weights = weights_dict(pn=0.3, nu=0.1, maxl=15)
    ml = 15
    ca = None
    plot = False
    debug = False
    svg = False
    strategy = 'dwithl'
    flowgram = parse_flowgram("A0 C1.0 G2.0 T3.0")
    reference = "AAAAAA ".encode()

    alignment, tuples = next(pflowalign(
                             flowgram,
                             reference,
                             plftau,
                             weights,
                             ml,
                             strategy,
                             ca,
                             plot,
                             debug,
                             svg))

    assert alignment.basecall().decode() == "CGGTTT"
    print(tuples)
    assert tuples == [(0, 1.0, 1, 0, 0.85730293798896851),
                      (2, 0.6561452775560469, 2, 2, 0.21335956732872149),
                      (2, 0.46514944327826852, 3, 4, 0.22133947559124992),
                      (3, 0.46920579316196226, 4, 7, 0.46920579316196226)]

    alignments = pflowalign(
        flowgram,
        reference,
        plftau,
        weights,
        ml,
        strategy,
        ca,
        plot,
        debug,
        svg)

    i = 0
    for alignment in alignments:
        _, _ = alignment
        i += 1
    assert i == 19
