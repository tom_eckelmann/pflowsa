#!/usr/bin/env python3

from pflowsa.pdicts import plftau_dict, weights_dict, compute_ps
from math import fsum
import os
from glob import glob
from pprint import pprint


def test_pfltau():
    plftau, plf, tau = plftau_dict()
    # 15635 items
    assert len(plftau) == len(plf) == 15635
    assert fsum(777897804 * plf[k] for k in plf.keys()) == 777897804
    # normalized
    assert fsum(plf[k] for k in plf.keys()) == 1.0

    # 0 0.09 24760238
    # 1 0.09 50
    # sum of all numbers in third row 777897804
    alln = 777897804
    f = 0.09
    al, an = 0, 24760238
    bl, bn = 1, 50

    aplf = an / alln
    bplf = bn / alln
    assert plf[(al, f)] == aplf  # 0.031829679776291024
    assert plf[(bl, f)] == bplf  # 6.427579528171544e-08

    tauf = aplf + bplf
    assert tau[f] == tauf

    aplftau = aplf / tauf
    bplftau = bplf / tauf
    assert plftau[(al, f)] == aplftau  # 0.9999979806373819
    assert plftau[(bl, f)] == bplftau  # 2.019362618076171e-06
    # normalized
    assert plftau[(al, f)] + plftau[(bl, f)] == 1.0


def test_compute_ps():
    ps = compute_ps(pn=0.3, nu=0.1)
    assert ps == 4 - 8 * 0.1 - 3 * 0.3

    ps = compute_ps(pn=0.03, nu=0.01)
    assert ps == 4 - 8 * 0.01 - 3 * 0.03


def test_weights():
    weights = weights_dict(pn=0.3, nu=0.1, maxl=15)
    # 120 shapes / 120 possible values per l, l in 1,...14
    # 15 shapes / 15 possible values for l = 0
    assert len(weights) == 1695
    assert weights[0, ""] == 1.0
    assert weights[1, "1"] == 2.3200000000000003
