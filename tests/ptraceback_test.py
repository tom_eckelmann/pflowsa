#!/usr/bin/env python3

from pflowsa.ptraceback import trace_opacities, find_reachable
import numpy as np


def test_pscoring():
    dbundles = np.empty((2, 3), dtype=np.ndarray)
    dbundles[0, 0] = [(-2, 1.0)]
    dbundles[1, 1] = [(1, 1.0)]
    dbundles[1, 2] = [(1, 0.01), (2, 0.99)]

    tracedbundle, pval = trace_opacities(dbundles)

    ttracedb = np.empty((2, 3), dtype=np.ndarray)
    ttracedb[0, 0] = [(-2, 1.0)]
    ttracedb[1, 2] = [(2, 0.99, 0.99 * 0.99)]

    print(tracedbundle[0, 0])
    assert tracedbundle[1, 2] == [(2, 0.99, 0 + 0.99 * 1.0)]
    assert tracedbundle[1, 1] is None
    assert tracedbundle[0, 0] == [(-2, 1.0, 1.0)]
    assert pval[1, 1] == 0
    assert pval[0, 0] == 2
    assert pval[1, 2] == 1


def test_find_reachable():
    dbundles = np.empty((3, 3), dtype=np.ndarray)
    dbundles[0, 0] = [(-2, 1.0, 1.0)]
    dbundles[1, 1] = [(1, 1.0, 1.0)]
    dbundles[1, 2] = [(1, 1.0, 1.0)]
    dbundles[2, 2] = [(1, 0.99, 1.0), (0, 0.3, 0.3)]
    pval = np.zeros((3, 3), dtype=float)
    pval[2, 2] = pval[1, 1] = pval[1, 2] = pval[0, 0] = 1
    print(dbundles)
    print(pval)
    reachdb, reachpval = find_reachable(dbundles, pval)
    assert reachpval[1, 2] == 0
