#!/home2/bio/software/local/bin/python
from __future__ import print_function

from Bio import SeqIO
import sys
from collections import Counter


for record in SeqIO.parse(sys.argv[1], "sff-trim"):
    print(">", record.id, sep='')
    print(record.seq)

# other interesting values:
# record.annotations["flow_values"]
# record.annotations["flow_chars"]
